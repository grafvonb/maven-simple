package de.dzbank.seu.ci.testprojekte.mvnsamplespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvnSampleSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvnSampleSpringbootApplication.class, args);
	}

}
